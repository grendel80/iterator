<?php

require_once 'config.php';
require_once 'getParameters.php';
require_once 'DbModel.php';
require_once 'DbArrayIterator.php';

//initial parameters
$perPage = 1000;
$tableName = 'users';

getParameters($perPage, $tableName);

try {
    $pdo = new PDO (
        $dbConfig['dsn'],
        $dbConfig['user'],
        $dbConfig['password']
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\PDOException $e) {
    die($e->getMessage());
}

$dbModel = new DbModel($pdo);

try {
    if ($dbModel->tableExists($tableName, $dbConfig['db_name'])) {

        $dataCount = $dbModel->getDataCount($tableName);

        $pages = ceil($dataCount / $perPage);

        for ($i = 0; $i <= $pages - 1; $i++) {
            $offset = $i * $perPage;
            $dbIterator = new DbArrayIterator($dbModel->getDataPageQuery($offset, $perPage, $tableName));

            foreach ($dbIterator as $key => $data) {
                // this data can be used to send email ect.
                echo 'I can do something with data on index:'. $key .PHP_EOL;
            }
        }
    } else {
        echo 'Table:' . $tableName . ' does not exist' . PHP_EOL;
    }
} catch (\PDOException $e) {
    echo 'PDO Exception: ' . $e->getMessage();
} catch (\Exception $e) {
    echo 'General Exception: ' . $e->getMessage();
}

