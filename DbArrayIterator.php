<?php

class DbArrayIterator implements Iterator
{
    protected $arr = [];
    protected $position;

    public function __construct(array $arr)
    {
        $this->arr = $arr;
        $this->position = 0;
    }

    /** {@inheritdoc} */
    public function current()
    {
        return $this->arr[$this->position];
    }

    /** {@inheritdoc} */
    public function next()
    {
        ++$this->position;
    }

    /** {@inheritdoc} */
    public function key()
    {
        return $this->position;
    }

    /** {@inheritdoc} */
    public function valid()
    {
        return isset($this->arr[$this->position]);
    }

    /** {@inheritdoc} */
    public function rewind()
    {
        $this->position = 0;
    }
}