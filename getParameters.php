<?php

function getParameters(&$perPage, &$tableName) {
    for ($i = 1; $i < $_SERVER['argc']; $i++) {
        switch ($_SERVER['argv'][$i]) {

            case '-t':
                // advance to the next argument
                $i++;
                if (isset($_SERVER['argv'][$i]) ) {
                    $tableName = $_SERVER['argv'][$i];
                    echo 'Selecting from: '. $tableName.PHP_EOL;
                } else {
                    // quit if no table specified
                    die("Specify table name after -t");
                }
                break;

            case '-p':
                // advance to the next argument
                $i++;
                if (isset($_SERVER['argv'][$i])  and ($_SERVER['argv'][$i] > 0) ) {
                    $perPage = $_SERVER['argv'][$i];
                    echo 'Per page limit is:'. $perPage. PHP_EOL;
                } else {
                    // quit if no filename specified
                    die("Specify per page limit after -p");
                }
                break;
            default:
                die('Unknown argument: '.$_SERVER['argv'][$i]);
                break;
        }
    }
}