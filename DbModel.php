<?php

class DbModel {

    protected $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Fetches data from table
     *
     * @param int $offset
     * @param int $limit
     * @param string $table
     * @return array
     * @throws PDOException
     */
    public function getDataPageQuery(int $offset = 0, int $limit = 1000, string $table): array
    {
        $query = $this->db->prepare( "SELECT * FROM $table LIMIT :offset, :limit");
        $query->bindParam(':offset', $offset, PDO::PARAM_INT);
        $query->bindParam(':limit', $limit, PDO::PARAM_INT);
        $query->execute();

        $result = $query->fetchAll(PDO::FETCH_OBJ);

        return $result;
    }

    /**
     * Count elements in table
     *
     * @param string $table
     * @return int
     * @throws PDOException
     */
    public function getDataCount(string $table): int
    {
        $result[0] = 0;

        $query = $this->db->prepare("SELECT COUNT(*) FROM $table");

        $query->execute();
        $result = $query->fetch();

        return intval($result[0]);
    }

    /**
     * Check if table exists in database
     *
     * @param string $table
     * @param string $db
     * @return bool
     * @throws PDOException
     */
    public function tableExists(string $table, string $db): bool
    {
        $query = $this->db->prepare('SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = :db AND table_name LIKE :table');
        $query->bindValue(':table', $table);
        $query->bindValue(':db', $db);
        $query->execute();

        $result = $query->fetch();

        return $result[0] == "1" ? true : false;
    }
}